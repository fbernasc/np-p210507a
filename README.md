# NP-P210507A

Behavioral data and code for "Neuroscience robotics for controlled induction and real-time assessment of hallucinations"

- Bernasconi, Blondiaux et al., 
- DOI: 10.1038/s41596-022-00737-z
- fosco.bernasconi@gmail.com 

# Repository organization:
- ./Data/ Behavioral data (associated with Figure 2)
- ./Code/ R codes for statistics and figure ploting (Figure 2)

# fMRI data:
- https://zenodo.org/record/4423384#.ZDe4Rs5BxmO

